Write-Host "Installing Windows components"
Write-Host "Installing da-DK language"
Install-Language -Language da-DK
Write-Host "Setting en-US as preferred UI language"
Set-SystemPreferredUILanguage -Language en-US

Write-Host "Installing IIS and selected sub components"
$installResult = Enable-WindowsOptionalFeature -Online -FeatureName IIS-WebServerRole, IIS-WebServer, IIS-CommonHttpFeatures, IIS-ManagementConsole, IIS-HttpErrors, IIS-HttpRedirect, IIS-WindowsAuthentication, IIS-StaticContent, IIS-DefaultDocument, IIS-HttpCompressionStatic, IIS-DirectoryBrowsing
if ($installResult.RestartNeeded) { 
    Write-Host "Restart needed."
    return
} 
else { 
    Write-Host "No restart needed." 
}
