# This script installs WinGet
# For a newer version of the script, see https://learn.microsoft.com/en-us/windows/package-manager/winget/

$progressPreference = 'silentlyContinue'
Write-Host "Downloading WinGet and its dependencies..."
#Omitting Desktop
Invoke-WebRequest -Uri https://aka.ms/getwinget -OutFile Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle
#Invoke-WebRequest -Uri https://aka.ms/Microsoft.VCLibs.x64.14.00.Desktop.appx -OutFile Microsoft.VCLibs.x64.14.00.Desktop.appx
Invoke-WebRequest -Uri https://github.com/microsoft/microsoft-ui-xaml/releases/download/v2.7.3/Microsoft.UI.Xaml.2.7.x64.appx -OutFile Microsoft.UI.Xaml.2.7.x64.appx
Write-Host "Installing WinGet and its dependencies..."
#Add-AppxPackage Microsoft.VCLibs.x64.14.00.Desktop.appx
Add-AppxPackage Microsoft.UI.Xaml.2.7.x64.appx
Add-AppxPackage Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle  

Remove-Item .\Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle
#Remove-Item .\Microsoft.VCLibs.x64.14.00.Desktop.appx
Remove-Item .\Microsoft.UI.Xaml.2.7.x64.appx
