﻿# README #

### Scripts for setting up a developer PC ###

The Powershell scripts in this repository are used to setup a PC with the prerequisites for doing software development.

Open a Powershell prompt and run all scripts as administrator.

* First ensure that Powershell scripts can be executed. Run this in an elevated Powerhell prompt.
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope Process -Force

When installing programs, start in this Git folder and use the following WinGet commands to search, install and add the line to the WinGet.ps1 file:
* winget search Brave
* winget install Brave.Brave

Then set up the PC, run the in this order scripts:
* 1WinGet.ps1
* 2BasicApplications.ps1
* 3WindowsSetup.ps1
* 4VisualStudio.ps1

Install these components manually
* winget install Microsoft.DotNet.SDK.7

<!-- Extra instructions for installed .NET components.
* Download dotnet-install.ps1 and install ARM version of .NET
Invoke-WebRequest -Uri https://dot.net/v1/dotnet-install.ps1 -OutFile dotnet-install.ps1
.\dotnet-install.ps1 -Channel 7.0 -Runtime dotnet
.\dotnet-install.ps1 -Channel 7.0 -Runtime aspnetcore
.\dotnet-install.ps1 -Channel 7.0 -Runtime windowsdesktop
See the documentation here> https://learn.microsoft.com/en-us/dotnet/core/tools/dotnet-install-script -->
