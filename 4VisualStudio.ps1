
$curDir = Get-Location
$configPath = "$curDir\Vs2022EnterprisePreview.vsconfig"
# The .vsconfig file is created with the Visual Studio Installer, accessible from Visual Studio Tools, Get Tools and Features..., More, Export Configuration 

Invoke-WebRequest -Uri https://aka.ms/vs/17/release/vs_enterprise.exe -OutFile vs_enterprise.exe
.\vs_enterprise.exe --config $configPath --channelUri https://aka.ms/vs/17/pre/channel --passive 
# ChannelUri: https://learn.microsoft.com/en-us/visualstudio/install/command-line-parameter-examples?view=vs-2022#using---channelUri

Remove-Item .\vs_enterprise.exe 
#winget install "Microsoft.VisualStudio.2022.Enterprise.Preview" --override "--passive --config $configPath"